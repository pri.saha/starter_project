package logger

import (
	"log"
	"os"
)

const (
	LOG_LEVEL_NONE = iota
	LOG_LEVEL_ERROR
	LOG_LEVEL_WARN
	LOG_LEVEL_INFO
	LOG_LEVEL_MAX
)

type AppLogger struct {
	infoLogger  *log.Logger
	warnLogger  *log.Logger
	errorLogger *log.Logger
	level       uint
}

func Initialize(level uint) *AppLogger {

	appLogger := &AppLogger{}

	appLogger.infoLogger = log.New(os.Stdout, "[INF] ", log.LstdFlags|log.Lmsgprefix|log.Lmicroseconds)
	appLogger.warnLogger = log.New(os.Stdout, "[WRN] ", log.LstdFlags|log.Lmsgprefix|log.Lmicroseconds)
	appLogger.errorLogger = log.New(os.Stdout, "[ERR] ", log.LstdFlags|log.Lmsgprefix|log.Lmicroseconds)

	if level >= LOG_LEVEL_MAX {
		level = LOG_LEVEL_MAX - 1
	}

	appLogger.level = level

	return appLogger
}

func (logger *AppLogger) Error(fmt string, args ...interface{}) {
	if logger.level != LOG_LEVEL_NONE && logger.level <= LOG_LEVEL_ERROR {
		logger.errorLogger.Printf(fmt+"\n", args...)
	}
}

func (logger *AppLogger) Warn(fmt string, args ...interface{}) {
	if logger.level != LOG_LEVEL_NONE && logger.level <= LOG_LEVEL_WARN {
		logger.warnLogger.Printf(fmt+"\n", args...)
	}
}

func (logger *AppLogger) Info(fmt string, args ...interface{}) {
	if logger.level != LOG_LEVEL_NONE && logger.level <= LOG_LEVEL_INFO {
		logger.infoLogger.Printf(fmt+"\n", args...)
	}
}
