package handlers

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"starter_project/internal/database"
	"starter_project/internal/logger"
	"starter_project/internal/models"

	"github.com/gorilla/mux"
)

const MAX_FORMDATA_SIZE int64 = 1024 * 1024 * 10

var appLogger *logger.AppLogger = nil

func InitializeLogger(l *logger.AppLogger) {
	appLogger = l
}

func GetPolicies(rsp http.ResponseWriter, req *http.Request) {

	err := req.ParseForm()

	if err != nil {
		rsp.WriteHeader(400)
		return
	}

	policyRequestData := models.PolicyRequestData{}

	policyRequestData.PolicyName = req.FormValue("name")
	policyRequestData.PolicyType = req.FormValue("type")
	policyRequestData.DeviceName = req.FormValue("device-name")

	err = policyRequestData.ValidateReadRequestPolicyData()

	if err != nil {
		appLogger.Error("%s", err.Error())
		rsp.WriteHeader(400)
		return
	}

	queryPolicy := policyRequestData.CreatePolicy()

	policies, _ := database.ReadPolicyAll(queryPolicy)

	rsp.WriteHeader(200)
	rsp.Header().Set("Content-Type", "application/json; charset=utf-8")

	for _, p := range policies {
		appLogger.Info("GET: Policy -> id: %d, name: %s, type: %s, version: %d, device: %s, checksum: %s",
			p.ID, p.PolicyName, p.PolicyType, p.Version, p.DeviceName, p.Checksum)
	}

	encoder := json.NewEncoder(rsp)
	encoder.Encode(&map[string][]models.Policy{"Policies": policies})
}

func GetPolicy(rsp http.ResponseWriter, req *http.Request) {

	err := req.ParseForm()

	if err != nil {
		rsp.WriteHeader(400)
		return
	}

	policyRequestData := models.PolicyRequestData{}

	policyRequestData.ID = mux.Vars(req)["id"]
	policyRequestData.PolicyName = req.FormValue("name")
	policyRequestData.PolicyType = req.FormValue("type")
	policyRequestData.DeviceName = req.FormValue("device-name")

	err = policyRequestData.ValidateReadRequestPolicyData()

	if err != nil {
		rsp.WriteHeader(400)
		return
	}

	queryPolicy := policyRequestData.CreatePolicy()

	p, _ := database.ReadPolicy(queryPolicy)

	rsp.WriteHeader(200)
	rsp.Header().Set("Content-Type", "application/json; charset=utf-8")

	appLogger.Info("GET: Policy -> id: %d, name: %s, type: %s, version: %d, device: %s, checksum: %s\n",
		p.ID, p.PolicyName, p.PolicyType, p.Version, p.DeviceName, p.Checksum)

	encoder := json.NewEncoder(rsp)
	encoder.Encode(p)
}

func CreatePolicy(rsp http.ResponseWriter, req *http.Request) {
	err := req.ParseMultipartForm(MAX_FORMDATA_SIZE)

	if err != nil {
		rsp.WriteHeader(400)
		return
	}

	policyRequestData := models.PolicyRequestData{}

	policyRequestData.PolicyName = req.FormValue("name")
	policyRequestData.PolicyType = req.FormValue("type")
	policyRequestData.DeviceName = req.FormValue("device-name")
	policyRequestData.Checksum = req.FormValue("checksum")

	fieldPolicyFile, _, _ := req.FormFile("policy")
	defer fieldPolicyFile.Close()
	data, _ := io.ReadAll(fieldPolicyFile)
	_ = data /* Suppress unusederror */

	//fmt.Printf("POST: Received data -> name: %s, type: %s, checksum: %s, device: %s\n",
	//	fieldName, fieldType, fieldChecksum, fieldDeviceName)

	err = policyRequestData.ValidateCreateRequestPolicyData()

	if err != nil {
		rsp.WriteHeader(400)
		return
	}

	policy := policyRequestData.CreatePolicy()
	policy.Version = 1

	_, err = database.CreatePolicy(policy)

	if err != nil {
		rsp.WriteHeader(500)
		return
	}

	fmt.Printf("POST: Policy created -> ID: %d, version: %d\n", policy.ID, policy.Version)
	rsp.WriteHeader(201)
	rsp.Header().Set("Content-Type", "application/json; charset=utf-8")

	encoder := json.NewEncoder(rsp)
	encoder.Encode(policy)
}

func UpdatePolicy(rsp http.ResponseWriter, req *http.Request) {
	err := req.ParseMultipartForm(MAX_FORMDATA_SIZE)

	if err != nil {
		rsp.WriteHeader(400)
		return
	}

	policyRequestData := models.PolicyRequestData{}

	policyRequestData.ID = mux.Vars(req)["id"]
	policyRequestData.PolicyName = req.FormValue("name")
	policyRequestData.PolicyType = req.FormValue("type")
	policyRequestData.DeviceName = req.FormValue("device-name")
	policyRequestData.Checksum = req.FormValue("checksum")

	err = policyRequestData.ValidateUpdateRequestPolicyData()

	if err != nil {
		rsp.WriteHeader(400)
		return
	}
	policyWithUpdatedValues := policyRequestData.CreatePolicy()

	policy := &models.Policy{}
	policy.ID = policyWithUpdatedValues.ID

	_, err = database.ReadPolicy(policy)

	if err != nil {
		rsp.WriteHeader(400)
		return
	}

	//fmt.Printf("PUT: Received data -> name: %s, type: %s, checksum: %s, device: %s\n",
	//	fieldName, fieldType, fieldChecksum, fieldDeviceName)

	has_changed := policy.UpdateFields(policyWithUpdatedValues)

	if has_changed {
		_, err = database.UpdatePolicy(policy)

		if err != nil {
			rsp.WriteHeader(500)
			return
		}
	}

	appLogger.Info("PUT: Policy updated -> ID: %d, version: %d\n", policy.ID, policy.Version)
	rsp.WriteHeader(200)
	rsp.Header().Set("Content-Type", "application/json; charset=utf-8")

	encoder := json.NewEncoder(rsp)
	encoder.Encode(policy)
}

func DeletePolicy(rsp http.ResponseWriter, req *http.Request) {

	err := req.ParseMultipartForm(MAX_FORMDATA_SIZE)

	if err != nil {
		rsp.WriteHeader(400)
		return
	}

	policyRequestData := models.PolicyRequestData{}

	policyRequestData.ID = req.FormValue("id")

	err = policyRequestData.ValidateDeleteRequestPolicyData()

	if err != nil {
		rsp.WriteHeader(400)
		return
	}

	policy := policyRequestData.CreatePolicy()

	//fmt.Printf("DELETE: Received data -> ID: %s\n", fieldID)

	err = database.DeletePolicy(policy)

	if err != nil {
		rsp.WriteHeader(400)
		return
	}

	appLogger.Info("DELETE: Policy deleted -> ID: %d\n", policy.ID)

	rsp.WriteHeader(204)
}
