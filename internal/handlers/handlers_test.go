package handlers

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"os"
	"starter_project/internal/database"
	"starter_project/internal/logger"
	"starter_project/internal/models"
	"testing"
)

func TestMain(m *testing.M) {

	appLogger := logger.Initialize(logger.LOG_LEVEL_INFO)

	database.Connect("postgres", "database", "5432", "postgres", "password", "waf_policy_db_test", appLogger)

	rc := m.Run()

	database.DeletePolicy(&models.Policy{})

	os.Exit(rc)
}

func helperPreparePolicyData() []models.Policy {
	policies := []models.Policy{
		{
			PolicyName: "policy_1",
			PolicyType: "security",
			DeviceName: "device-name=waf-editor01.lon1.defense.net",
			Checksum:   "ca6905f8ea190c798b30e664d96880c8641af405fc429c0d76feaca6c9e3764d",
		},
		{
			PolicyName: "policy_2",
			PolicyType: "security",
			DeviceName: "device-name=waf-editor01.lon2.defense.net",
			Checksum:   "ca6905f8ea190c798b30e664d96880c8642af405fc429c0d76feaca6c9e3764d",
		},
		{
			PolicyName: "policy_3",
			PolicyType: "security",
			DeviceName: "device-name=waf-editor01.lon3.defense.net",
			Checksum:   "ca6905f8ea190c798b30e664d96880c8643af405fc429c0d76feaca6c9e3764d",
		},
		{
			PolicyName: "policy_4",
			PolicyType: "security",
			DeviceName: "device-name=waf-editor01.lon4.defense.net",
			Checksum:   "ca6905f8ea190c798b30e664d96880c8644kaf405fc429c0d76feaca6c9e3764d",
		},
	}

	policiesCopy := make([]models.Policy, len(policies))

	copy(policiesCopy, policies)

	return policiesCopy
}

func helperAddPolicies() map[uint]models.Policy {

	policies := helperPreparePolicyData()

	for _, policy := range policies {
		database.CreatePolicy(&policy)
	}

	policyMap := map[uint]models.Policy{}

	for _, policy := range policies {
		policyMap[policy.ID] = policy
	}

	return policyMap
}

func helperCheckStatusCode(t *testing.T, expected, actual int) {
	if actual != expected {
		t.Errorf("Expected response code %d. Got %d\n", expected, actual)
	}
}

func helperDeletePolicies() {
	database.DeletePolicy(&models.Policy{})
}

func helperVerifyPolicyResponse(t *testing.T, addedPolicies map[uint]models.Policy, policies []models.Policy) {

	var found bool

	if len(addedPolicies) != len(policies) {
		t.Errorf("Number of policies added != Number of policies fetched\n")
	}

	for _, policy := range addedPolicies {
		found = false
		for _, p := range policies {
			if policy.ID == p.ID {
				found = true

				if policy.PolicyName != p.PolicyName ||
					policy.PolicyType != p.PolicyType ||
					policy.DeviceName != p.DeviceName ||
					policy.Checksum != p.Checksum ||
					policy.Version != p.Version {
					t.Errorf("Policy attributes with ID %d failed to match in fetched policy list\n", policy.ID)
				}
			}
		}

		if !found {
			t.Errorf("Policy with ID %d not found in fetched policy list\n", policy.ID)
		}
	}
}

func TestGetPolicies(t *testing.T) {

	var policies map[string][]models.Policy

	policyMap := helperAddPolicies()

	req, _ := http.NewRequest("GET", "/api/v1/policies", nil)
	rsp := httptest.NewRecorder()

	GetPolicies(rsp, req)

	helperCheckStatusCode(t, http.StatusOK, rsp.Code)

	json.NewDecoder(rsp.Body).Decode(&policies)

	helperVerifyPolicyResponse(t, policyMap, policies["Policies"])

	helperDeletePolicies()
}

func TestGetPolicy(t *testing.T) {

	policyMap := helperAddPolicies()

	p := models.Policy{}

	for _, policy := range policyMap {
		req, _ := http.NewRequest("GET", fmt.Sprintf("/api/v1/policies/%d", policy.ID), nil)
		rsp := httptest.NewRecorder()

		GetPolicy(rsp, req)

		helperCheckStatusCode(t, http.StatusOK, rsp.Code)

		json.NewDecoder(rsp.Body).Decode(&p)

		helperVerifyPolicyResponse(t, map[uint]models.Policy{policy.ID: policy}, []models.Policy{p})
	}

	helperDeletePolicies()
}

func TestCreatePolicy(t *testing.T) {

	helperDeletePolicies()

	policies := helperPreparePolicyData()

	p := models.Policy{}

	for _, policy := range policies {
		req, _ := http.NewRequest("POST",
			fmt.Sprintf("/api/v1/policies?name=%s&type=%s&device-name=%s&checksum=%s",
				policy.PolicyName, policy.PolicyType, policy.DeviceName, policy.Checksum), nil)

		rsp := httptest.NewRecorder()

		CreatePolicy(rsp, req)

		helperCheckStatusCode(t, http.StatusCreated, rsp.Code)

		json.NewDecoder(rsp.Body).Decode(&p)

		policy.ID = p.ID

		helperVerifyPolicyResponse(t, map[uint]models.Policy{policy.ID: policy}, []models.Policy{p})
	}

	helperDeletePolicies()
}

func TestUpdatePolicy(t *testing.T) {

	policyMap := helperAddPolicies()

	p := models.Policy{}

	for _, policy := range policyMap {
		req, _ := http.NewRequest("PUT", fmt.Sprintf("/api/v1/policies/%d?name=policy_%d_new", policy.ID, policy.ID), nil)
		rsp := httptest.NewRecorder()

		GetPolicy(rsp, req)

		helperCheckStatusCode(t, http.StatusOK, rsp.Code)

		json.NewDecoder(rsp.Body).Decode(&p)

		policy.PolicyName = fmt.Sprintf("policy_%d_new", policy.ID)

		helperVerifyPolicyResponse(t, map[uint]models.Policy{policy.ID: policy}, []models.Policy{p})

		break
	}

	helperDeletePolicies()
}

func TestDeletePolicy(t *testing.T) {

	policyMap := helperAddPolicies()

	p := models.Policy{}

	for _, policy := range policyMap {
		req, _ := http.NewRequest("DELETE", fmt.Sprintf("/api/v1/policies/%d", policy.ID), nil)
		rsp := httptest.NewRecorder()

		GetPolicy(rsp, req)

		helperCheckStatusCode(t, http.StatusNoContent, rsp.Code)

		json.NewDecoder(rsp.Body).Decode(&p)

		helperVerifyPolicyResponse(t, map[uint]models.Policy{policy.ID: policy}, []models.Policy{p})
	}

	helperDeletePolicies()
}
