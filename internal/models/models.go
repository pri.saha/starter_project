package models

import (
	"fmt"
	"strconv"
	"strings"

	"gorm.io/gorm"
)

type Policy struct {
	gorm.Model
	PolicyName string
	PolicyType string
	Checksum   string
	DeviceName string
	Version    uint
}

type constraintChecker func() error

type validator struct {
	constraints []constraintChecker
}

const (
	POLICY_NAME_STR_MAX_LEN = 32
	POLICY_TYPE_STR_MAX_LEN = 20
	DEVICE_NAME_STR_MAX_LEN = 100
	CHECKSUM_STR_MAX_LEN    = 32
)

type PolicyRequestData struct {
	ID         string
	PolicyName string
	PolicyType string
	Checksum   string
	DeviceName string
}

func (validator *validator) validate() error {

	errors := []string{}

	for _, constraint := range validator.constraints {
		err := constraint()

		if err != nil {
			errors = append(errors, err.Error())
		}
	}

	if len(errors) == 0 {
		return nil
	}

	return fmt.Errorf(strings.Join(errors, ","))
}

func nonNullConstraint(value, fieldName string) constraintChecker {
	return func() error {
		if strings.TrimSpace(value) == "" {
			return fmt.Errorf("paramater %s cannot be empty", fieldName)
		}
		return nil
	}
}

func postiiveNonZeroConstraint(value string, fieldName string) constraintChecker {
	return func() error {

		if strings.TrimSpace(value) == "" {
			return nil
		}

		number, err := strconv.Atoi(value)

		if err != nil {
			return fmt.Errorf("parameter %s: '%s' is not a number", fieldName, value)
		}

		if number <= 0 {
			return fmt.Errorf("parameter %s: '%s' cannot be <= 0", fieldName, value)
		}

		return nil
	}
}

func maxLenConstraint(value, fieldName string, maxLength int) constraintChecker {
	return func() error {
		if len(value) > maxLength {
			return fmt.Errorf("parameter %s: '%s' length cannot be > %d", fieldName, value, maxLength)
		}
		return nil
	}
}

func (validator *validator) addConstraint(f constraintChecker) {
	validator.constraints = append(validator.constraints, f)
}

func (policy *PolicyRequestData) preprocess() {

	policy.ID = strings.TrimSpace(policy.ID)
	policy.PolicyName = strings.TrimSpace(policy.PolicyName)
	policy.PolicyType = strings.TrimSpace(policy.PolicyType)
	policy.DeviceName = strings.TrimSpace(policy.DeviceName)
	policy.Checksum = strings.TrimSpace(policy.Checksum)
}

func (policy *PolicyRequestData) ValidateCreateRequestPolicyData() error {

	validator := validator{}

	policy.preprocess()

	validator.addConstraint(nonNullConstraint(policy.PolicyName, "policy-name"))
	validator.addConstraint(nonNullConstraint(policy.PolicyType, "policy-type"))
	validator.addConstraint(nonNullConstraint(policy.DeviceName, "device-name"))
	validator.addConstraint(nonNullConstraint(policy.Checksum, "checksum"))
	validator.addConstraint(maxLenConstraint(policy.PolicyName, "policy-name", POLICY_NAME_STR_MAX_LEN))
	validator.addConstraint(maxLenConstraint(policy.PolicyType, "policy-type", POLICY_TYPE_STR_MAX_LEN))
	validator.addConstraint(maxLenConstraint(policy.DeviceName, "device-name", DEVICE_NAME_STR_MAX_LEN))
	validator.addConstraint(maxLenConstraint(policy.Checksum, "checksum", CHECKSUM_STR_MAX_LEN))

	return validator.validate()
}

func (policy *PolicyRequestData) ValidateReadRequestPolicyData() error {

	validator := validator{}

	policy.preprocess()

	validator.addConstraint(postiiveNonZeroConstraint(policy.ID, "id"))
	validator.addConstraint(maxLenConstraint(policy.PolicyName, "policy-name", POLICY_NAME_STR_MAX_LEN))
	validator.addConstraint(maxLenConstraint(policy.PolicyType, "policy-type", POLICY_TYPE_STR_MAX_LEN))
	validator.addConstraint(maxLenConstraint(policy.DeviceName, "device-name", DEVICE_NAME_STR_MAX_LEN))

	return validator.validate()
}

func (policy *PolicyRequestData) ValidateUpdateRequestPolicyData() error {

	validator := validator{}

	policy.preprocess()

	validator.addConstraint(nonNullConstraint(policy.ID, "id"))
	validator.addConstraint(postiiveNonZeroConstraint(policy.ID, "id"))
	validator.addConstraint(maxLenConstraint(policy.PolicyName, "policy-name", POLICY_NAME_STR_MAX_LEN))
	validator.addConstraint(maxLenConstraint(policy.PolicyType, "policy-type", POLICY_TYPE_STR_MAX_LEN))
	validator.addConstraint(maxLenConstraint(policy.DeviceName, "device-name", DEVICE_NAME_STR_MAX_LEN))
	validator.addConstraint(maxLenConstraint(policy.Checksum, "checksum", CHECKSUM_STR_MAX_LEN))

	return validator.validate()
}

func (policy *PolicyRequestData) ValidateDeleteRequestPolicyData() error {

	validator := validator{}

	policy.preprocess()

	validator.addConstraint(nonNullConstraint(policy.ID, "id"))
	validator.addConstraint(postiiveNonZeroConstraint(policy.ID, "id"))

	return validator.validate()
}

func (policyRequestData *PolicyRequestData) CreatePolicy() *Policy {

	policy := &Policy{}

	if policyRequestData.ID != "" {
		value, _ := strconv.Atoi(policyRequestData.ID)
		policy.ID = uint(value)
	}

	if policyRequestData.PolicyName != "" {
		policy.PolicyName = policyRequestData.PolicyName
	}

	if policyRequestData.PolicyName != "" {
		policy.PolicyType = policyRequestData.PolicyType
	}

	if policyRequestData.DeviceName != "" {
		policy.DeviceName = policyRequestData.DeviceName
	}
	if policyRequestData.Checksum != "" {
		policy.Checksum = policyRequestData.Checksum
	}

	return policy
}

func (policy *Policy) UpdateFields(policyWithUpdatedValues *Policy) bool {

	has_changed := false

	if policyWithUpdatedValues.PolicyName != "" {
		if policy.PolicyName != policyWithUpdatedValues.PolicyName {
			policy.PolicyName = policyWithUpdatedValues.PolicyName
			has_changed = true
		}
	}

	if policyWithUpdatedValues.PolicyType != "" {
		if policy.PolicyType != policyWithUpdatedValues.PolicyType {
			policy.PolicyType = policyWithUpdatedValues.PolicyType
			has_changed = true
		}
	}

	if policyWithUpdatedValues.DeviceName != "" {
		if policy.DeviceName != policyWithUpdatedValues.DeviceName {
			policy.DeviceName = policyWithUpdatedValues.DeviceName
			has_changed = true
		}
	}

	if policyWithUpdatedValues.Checksum != "" {
		if policy.Checksum != policyWithUpdatedValues.Checksum {
			policy.Checksum = policyWithUpdatedValues.Checksum
			has_changed = true
		}
	}

	if has_changed {
		policy.Version += 1
	}

	return has_changed
}
