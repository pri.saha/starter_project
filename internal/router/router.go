package router

import (
	"starter_project/internal/handlers"
	"starter_project/internal/logger"

	"github.com/gorilla/mux"
)

func Register(appLogger *logger.AppLogger) *mux.Router {

	handlers.InitializeLogger(appLogger)

	router := mux.NewRouter()

	router.HandleFunc("/api/v1/policies", handlers.GetPolicies).Methods("GET")
	router.HandleFunc("/api/v1/policies/{id}", handlers.GetPolicy).Methods("GET")
	router.HandleFunc("/api/v1/policies", handlers.CreatePolicy).Methods("POST")
	router.HandleFunc("/api/v1/policies/{id}", handlers.UpdatePolicy).Methods("PUT")
	router.HandleFunc("/api/v1/policies/{id}", handlers.DeletePolicy).Methods("DELETE")

	appLogger.Info("Server url routes registration [SUCCESS]")

	return router
}
