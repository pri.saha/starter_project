package database

import (
	"fmt"
	"os"
	"starter_project/internal/logger"
	"starter_project/internal/models"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	gorm_logger "gorm.io/gorm/logger"
	"gorm.io/gorm/schema"
)

var db *gorm.DB

func Connect(dbApp, host, port, user, password, dbName string, appLogger *logger.AppLogger) {

	var err error

	connStr := fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=disable",
		host, port, user, password, dbName)

	switch dbApp {
	case "postgres":
		db, err = gorm.Open(postgres.Open(connStr), &gorm.Config{
			NamingStrategy: schema.NamingStrategy{
				TablePrefix:   "starter_project.",
				SingularTable: false,
			},
			Logger: gorm_logger.Default.LogMode(gorm_logger.Info),
		})
	default:
		appLogger.Error("unsupported database application: %s", dbApp)
	}

	if err != nil {
		appLogger.Error("Database connect [FAILED]: %s", err.Error())
		os.Exit(1)
	}

	appLogger.Info("Database connect [SUCCESS]")

	db.Logger = gorm_logger.Default.LogMode(gorm_logger.Info)
}

func CreatePolicy(policy *models.Policy) (*models.Policy, error) {

	err := db.Create(policy).Error
	return policy, err
}

func ReadPolicyAll(policy *models.Policy) ([]models.Policy, error) {

	policies := []models.Policy{}

	err := db.Where(policy).Find(&policies).Error
	return policies, err
}

func ReadPolicy(policy *models.Policy) (*models.Policy, error) {

	err := db.First(policy).Error
	return policy, err
}

func UpdatePolicy(policy *models.Policy) (*models.Policy, error) {

	err := db.Model(policy).Updates(*policy).Error
	return policy, err
}

func DeletePolicy(policy *models.Policy) error {

	return db.Unscoped().Delete(&policy).Error
}
