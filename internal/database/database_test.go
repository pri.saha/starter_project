package database

import (
	"os"
	"starter_project/internal/logger"
	"starter_project/internal/models"
	"testing"
)

func TestMain(m *testing.M) {

	appLogger := logger.Initialize(logger.LOG_LEVEL_INFO)

	Connect("postgres", "database", "5432", "postgres", "password", "waf_policy_db_test", appLogger)

	rc := m.Run()

	DeletePolicy(&models.Policy{})

	os.Exit(rc)
}

func TestCreatePolicy(t *testing.T) {

	policy := &models.Policy{
		PolicyName: "policy_1",
		PolicyType: "security",
		DeviceName: "waf-editor01.lon1.defense.net",
		Checksum:   "ca6905f8ea190c798b30e664d96880c8641af405fc429c0d76feaca6c9e3764d",
	}

	_, err := CreatePolicy(policy)

	if err != nil {
		t.Errorf("test failed")
	}

}

func TestUpdatePolicy(t *testing.T) {

	policy := &models.Policy{
		PolicyName: "policy_2",
		PolicyType: "security",
		DeviceName: "waf-editor02.lon1.defense.net",
		Checksum:   "ca6905f8ea190c798b30e664d96880c8641af405fc429c0d76feaca6c9e3764d",
	}

	_, err := CreatePolicy(policy)

	if err != nil {
		t.Errorf("test failed")
	}

	policy.PolicyName = "policy_1_new"
	policy.Version += 1

	_, err = UpdatePolicy(policy)

	if err != nil {
		t.Errorf("test failed")
	}
}

func TestDeletePolicy(t *testing.T) {

	policy := &models.Policy{
		PolicyName: "policy_3",
		PolicyType: "security",
		DeviceName: "waf-editor03.lon1.defense.net",
		Checksum:   "ca6905f8ea190c798b30e664d96880c8641af405fc429c0d76feaca6c9e3764d",
	}

	_, err := CreatePolicy(policy)

	if err != nil {
		t.Errorf("test failed")
	}

	err = DeletePolicy(policy)

	if err != nil {
		t.Errorf("test failed")
	}
}
