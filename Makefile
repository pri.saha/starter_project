.PHONY: up
up:
	@docker-compose up --build

.PHONY: down
down:
	@docker-compose down

.PHONY: migration-docker-build
migration-docker-build:
	@docker build -t starter-project-db-migration -f docker/migration/Dockerfile .

.PHONY: migration-up
migration-up:
	@docker run -t --rm --network host \
		-e "DB_HOST=127.0.0.1" \
		-e "DB_USER=postgres" \
		-e "DB_PASS=password" \
		-e "DB_SSL_MODE=disable" \
		--entrypoint /app/tools/migration-up.sh \
	    starter-project-db-migration

.PHONY: migration-down
migration-down:
	@docker run -t --rm --network host \
		-e "DB_HOST=127.0.0.1" \
		-e "DB_USER=postgres" \
		-e "DB_PASS=password" \
		-e "DB_SSL_MODE=disable" \
		--entrypoint /app/tools/migration-down.sh \
		starter-project-db-migration