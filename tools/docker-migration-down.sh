#!/bin/sh

# Stop processing if a non-zero error code is hit
set -e

# Init vars with defaults
DB_USER="${DB_USER:-}"
DB_PASS="${DB_PASS:-}"
DB_HOST="${DB_HOST:-}"
DB_PORT="${DB_PORT:-5432}"
DB_NAME="${DB_NAME:-waf_policy_db}"
DB_MIGRATION_PATH="${DB_MIGRATION_PATH:-/app/migrations}"
DB_SSL_MODE="${DB_SSL_MODE:-require}"

# Encode risky parameters
DB_USER=$(echo "${DB_USER}" | jq "@uri" -jRr)
DB_PASS=$(echo "${DB_PASS}" | jq "@uri" -jRr)

# Construct postgres uri
DB_URI="postgres://${DB_USER}:${DB_PASS}@${DB_HOST}:${DB_PORT}/${DB_NAME}?sslmode=${DB_SSL_MODE}"

# Wait for postgres to be available
WAIT_ATTEMPTS=0
while ! nc -z "$DB_HOST" "$DB_PORT"; do
  WAIT_ATTEMPTS=$((WAIT_ATTEMPTS+1))
  if [ $WAIT_ATTEMPTS -gt 4 ]; then
    echo "error: timed out waiting for postgres" && exit 1
  fi
  sleep 1
done

# Run db migrations
migrate -path="$DB_MIGRATION_PATH" -database "$DB_URI" down 1