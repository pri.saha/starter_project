index=1

while [ $index -le 5 ]
do
    curl \
      -F "name=policy_${index}" \
      -F "type=security" \
      -F "device-name=waf-editor0${index}.lon1.defense.net" \
      -F "checksum=ca6905f8ea190c798b30e664d96880c864${index}af405fc429c0d76feaca6c9e3764d" \
      -F "policy=@/Users/pri.saha/Downloads/dummy_policy.xml" \
      http://localhost:8080/api/v1/policies | python3 -m json.tool

    index=$(expr $index + 1)
done
