BEGIN;

-- drop tables

DROP TABLE IF EXISTS starter_project.policies;

-- drop types

DROP TYPE IF EXISTS starter_project.policy_type;

END;