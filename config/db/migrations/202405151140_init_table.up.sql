BEGIN;

-- create types

DO $$
BEGIN
    IF NOT EXISTS (SELECT 1 FROM pg_type WHERE typname = 'starter_project.policy_type' ) THEN
        CREATE TYPE starter_project.policy_type AS ENUM ('security', 'routing');
    END IF;
END$$;

-- create tables

CREATE TABLE IF NOT EXISTS starter_project.policies (
    id BIGSERIAL PRIMARY KEY NOT NULL,
    created_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT current_timestamp,
    updated_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT current_timestamp,
    deleted_at TIMESTAMP WITH TIME ZONE,
    policy_name TEXT NOT NULL,
    policy_type starter_project.policy_type NOT NULL,
    checksum TEXT NOT NULL,
    device_name TEXT NOT NULL,
    version BIGINT NOT NULL
);

COMMIT;