package main

import (
	"log"
	"net/http"
	"os"
	"starter_project/internal/database"
	"starter_project/internal/logger"
	"starter_project/internal/router"
)

func main() {

	dbUser := os.Getenv("DB_USER")
	dbPassword := os.Getenv("DB_PASSWORD")
	dbName := os.Getenv("DB_NAME")
	dbApp := os.Getenv("DB_APP")
	dbHost := os.Getenv("DB_HOST")
	dbPort := os.Getenv("DB_PORT")

	appLogger := logger.Initialize(logger.LOG_LEVEL_INFO)

	database.Connect(dbApp, dbHost, dbPort, dbUser, dbPassword, dbName, appLogger)

	router := router.Register(appLogger)

	appLogger.Info("Server running on port [:8000]")
	log.Fatal(http.ListenAndServe(":8000", router))
}
